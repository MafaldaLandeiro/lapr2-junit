
package complexos;

import java.util.Comparator;

/**
 *
 * @author Angelo Martins
 */

public class Complexos implements Comparable<Complexos> {
    double a, b;

    Complexos(double a, double b) {
        this.a=a;
        this.b=b;
    }
    Complexos() {
        this.a=0;
        this.b=0;
    }

    public Complexos add(Complexos z) {
       Complexos c = new Complexos();
       c.a = z.a + this.a;
       c.b = z.b + this.b;
       return c;
    }

    public Complexos multiply(Complexos z) {
       Complexos c = new Complexos();
       c.a = z.a * this.a - z.b * this.b;
       c.b = z.a * this.b + z.b * this.a;
       return c;
    }
    
    public double getModulo() {
        return Math.sqrt(this.a*this.a + this.b*this.b);
    }
    
    @Override
    public int compareTo(Complexos c) {
        
        double res = this.getModulo() - c.getModulo();
        
        if(res > 0) {
            return 1;
        }
        else if(res<0) {
            return -1;  
        }
        else
            return 0;
    }
    
    /**
     * 
     */
    public static Comparator<Complexos> ComplexosImagComparator
            = new Comparator<Complexos>() {
                
       @Override
       public int compare(Complexos c1, Complexos c2) {
           if(c1.b>=c2.b)
               return 1;
           else
               return -1;
       }
                
    };
    
    @Override
    public String toString() {
        return "(" + a + " + i" + b + ")";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object other){
        boolean result = false;
        if (other instanceof Complexos) {
            Complexos that = (Complexos) other;
            result = (this.a == that.a && this.b == that.b);
        }
        return result;
    }
} 
